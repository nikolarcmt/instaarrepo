﻿using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Utils;
using TMPro;
using MiniJSON;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DropDown : MonoBehaviour {

    public AbstractMap _map;

    Resolution[] resolutions;

    [SerializeField] GameObject buttonPrefab;

    [SerializeField] Transform menuPanel;

    public static string choosedLocation;

    public static List<Vector2d> vectorDDLatLng = new List<Vector2d>();

    public GameObject screen2;
    public GameObject backButton;

    public static string readAccToken;

    // Use this for initialization
    void Start () {
        
        
	}

    public void OpenGeoCordinates()
    {

        foreach (Transform child in menuPanel.transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < Instagramgetter.nameLatLng.Count; i++)
        {
            GameObject button = (GameObject)Instantiate(buttonPrefab);
           // button.GetComponentInChildren<Text>().text = ResToString(resolutions[i]);
            button.GetComponentInChildren<Text>().text = Instagramgetter.nameLatLng[i];

            int index = i;
            button.GetComponent<Button>().onClick.AddListener(() => { SetResolution(index); });
            button.transform.parent = menuPanel;
            
        }

        //StartCoroutine(DrowInstaPictures());
    }


    void SetResolution(int index)
    {
        choosedLocation = Instagramgetter.locationsLatLng[index];
        
        Debug.Log("SSS" + choosedLocation);
        //Screen.SetResolution(resolutions[index].width, resolutions[index].height, false);
        foreach (Transform child in menuPanel.transform)
        {
            Destroy(child.gameObject);
        }

        vectorDDLatLng = Instagramgetter.vectorLatLng;

        //Vector2d latLon = new Vector2d(48.6803404, 1.9421879);

        _map.UpdateMap(vectorDDLatLng[index], 17);

        StartCoroutine(DrowInstaPictures(vectorDDLatLng[index]));

    }

    string ResToString(Resolution res)
    {
        return res.width + " x " + res.height;
    }



    public void LoadLatLng()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            //compassValTMP = data.accToken; //ovde smesti acc token
            readAccToken = data.accToken;
        }
    }


    IEnumerator DrowInstaPictures(Vector2d tmpVector)
    {
        double tmpLat = tmpVector.x;
        double tmpLng = tmpVector.y;

        LoadLatLng();

        string url1 = "https://api.instagram.com/v1/media/search?lat=" + tmpLat + "&lng=" + tmpLng + "&distance=100000&access_token=" + readAccToken;

       
        WWW www = new WWW(url1);
        //43.3277501,21.9015916
        yield return www;

        string api_response = www.text;
        Debug.Log(api_response);

        IDictionary apiParse = (IDictionary)Json.Deserialize(api_response);
        IList instagramPicturesList = (IList)apiParse["data"];

        Debug.Log("Ovo su prave VESTI" + instagramPicturesList.Count);

        foreach (IDictionary instagramPicture in instagramPicturesList)
        {
            
            //main picture info
            IDictionary images = (IDictionary)instagramPicture["images"];
            IDictionary standardResolution = (IDictionary)images["standard_resolution"];
            string mainPic_url = (string)standardResolution["url"];
            //Debug.Log(mainPic_url);

            //location info
            WWW mainPic = new WWW(mainPic_url);
            yield return mainPic;
            IDictionary location = (IDictionary)instagramPicture["location"];
            

            double lat = (double)location["latitude"];
            double lon = (double)location["longitude"];
            string placeName = (string)location["name"];
            Debug.Log("Ovo su KORDINATE" + lat + ", " + lon);



            GameObject instaPost = Instantiate(Resources.Load("InstaPost")) as GameObject; //ovo ovde da se postavi blesing
            instaPost.transform.GetChild(0).GetComponent<MeshRenderer>().material.mainTexture = mainPic.texture;

            instaPost.transform.position = _map.GeoToWorldPosition(new Mapbox.Utils.Vector2d(lat, lon)) + new Vector3(0, 0.15f, 0);
            instaPost.transform.SetParent(_map.transform);
            //instaPost.transform.localScale = new Vector3(10,10,1);


            IDictionary user = (IDictionary)instagramPicture["user"];
            string userName = (string)user["username"];
            string profilePicture_url = (string)user["profile_picture"];

            WWW profilePic = new WWW(profilePicture_url);
            yield return profilePic;
            instaPost.transform.GetChild(1).GetComponent<MeshRenderer>().material.mainTexture = profilePic.texture;
            instaPost.transform.GetChild(2).GetComponent<TextMeshPro>().text = userName;

            //location name
            string placeNameTmp = (string)location["name"];
            instaPost.transform.GetChild(3).GetComponent<TextMeshPro>().text = placeNameTmp;

            //likes
            IDictionary Likes = (IDictionary)instagramPicture["likes"];
            string likes = (string)Likes["count"].ToString();
            instaPost.transform.GetChild(4).GetComponent<TextMeshPro>().text = likes + "likes";
            
        }

        _map.transform.position = new Vector3(0, 0, 0);

        ///ovdeeeeeeeasdasdasd
        screen2.SetActive(false);
        backButton.SetActive(true);

    }
    // Update is called once per frame
    void Update () {
		
	}
}
