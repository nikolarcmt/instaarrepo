﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public static class StringResources
    {
        public static string AccessTokenUrl = "https://api.instagram.com/oauth/access_token";
        public static string GetUserDataUrl = "https://api.instagram.com/v1/users/self/media/recent/?max_id=200&access_token=";

        public static string InstagramLoginPage = "https://api.instagram.com/oauth/authorize/?client_id=acf97131561e4f4ca96530d5c9e57ea7&redirect_uri=http://localhost:5000/&response_type=code&scope=basic+likes+relationships+public_content";

    }
}
