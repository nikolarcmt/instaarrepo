﻿using Assets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts;

public class AccessTokenRequest : MonoBehaviour {



    public GameObject Screen1;
    public GameObject Screen2;

    public static bool screenShow = false;


    [Serializable]
    public class TokenClassName
    {
        public string access_token;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!string.IsNullOrEmpty(TokenDataHolder.urlCode))
        {
            Debug.Log("Class RequestAccessToken call: " + TokenDataHolder.urlCode);

            StartCoroutine(GetAccessToken(TokenDataHolder.urlCode));
            TokenDataHolder.urlCode = string.Empty;
        }

        if (screenShow)
        {
            Screen2.SetActive(true);
            Screen1.SetActive(false);
            if (TokenDataHolder.changeScreen == false)
                TokenDataHolder.changeScreen = true;

            screenShow = false;
            
        }
        
    }

    private static IEnumerator GetAccessToken(string code)
    {
        Dictionary<string, string> content = new Dictionary<string, string>();
        //Fill key and value
        content.Add("client_id", "acf97131561e4f4ca96530d5c9e57ea7");
        content.Add("client_secret", "4a5c37080cb94a3cb6cdd2cecc28fb9d");
        content.Add("redirect_uri", "http://localhost:5000/");
        content.Add("grant_type", "authorization_code");
        content.Add("code", code);

        //Debug.Log("Before www request");

        //UnityWebRequest www = UnityWebRequest.Post("https://api.instagram.com/oauth/access_token", content);
        UnityWebRequest www = UnityWebRequest.Post(StringResources.AccessTokenUrl, content);

        //Send request
        yield return www.SendWebRequest();

        if (!www.isNetworkError)
        {
            string resultContent = www.downloadHandler.text;
            TokenClassName json = JsonUtility.FromJson<TokenClassName>(resultContent);

            Debug.Log(resultContent);
            Debug.Log("AccessToken= " + json.access_token);


            //Debug.Log("GetAccessTokenFunction if");

            SaveAccessToken(json.access_token);

            if (InAppBrowser.IsInAppBrowserOpened())
            {
                InAppBrowser.ClearCache();
                InAppBrowser.CloseBrowser();
            }

        }
        else
        {
            //Return null
            //Debug.Log("GetAccessTokenFunction else");
        }
    }




    public static void SaveAccessToken(string tmpAccToken)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData();
        data.accToken = tmpAccToken;
      

        bf.Serialize(file, data);
        file.Close();
        Debug.Log("DONE");
        screenShow = true;
    }
}

[Serializable]
class PlayerData
{
    public string accToken;
}
