﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebUrl : MonoBehaviour {

    public GameObject screen2;
    public GameObject backButton;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void btnOne()
    {
        Application.OpenURL("http://localhost:5000");
    }

    public void btnTwo()
    {
        //Application.OpenURL("https://api.instagram.com/oauth/authorize/?client_id=acf97131561e4f4ca96530d5c9e57ea7&redirect_uri=http://localhost:5000/&response_type=code&scope=basic+likes+relationships+public_content");
        //string pageToOpen = "https://api.instagram.com/oauth/authorize/?client_id=acf97131561e4f4ca96530d5c9e57ea7&redirect_uri=http://localhost:5000/&response_type=code&scope=basic+likes+relationships+public_content";

        InAppBrowser.DisplayOptions options = new InAppBrowser.DisplayOptions();
        options.displayURLAsPageTitle = false;
        options.pageTitle = "Instagram login";

       
        string pageToOpen = StringResources.InstagramLoginPage;
        InAppBrowser.OpenURL(pageToOpen, options);

    }

    public void btnBack()
    {
        screen2.SetActive(true);
        backButton.SetActive(false);
    }
}
