﻿using Assets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using UnityEngine;

public class CodeRequest : MonoBehaviour {

    public static HttpListener _httpListener = new HttpListener();
    public bool started = false;
    private static string sample = "code=";

    // Use this for initialization
    void Start () {
        Debug.Log("Starting server...");
        _httpListener.Prefixes.Add("http://localhost:5000/");
        _httpListener.Start();
        Console.WriteLine("Server started.");
        Thread _responseThread = new Thread(ResponseThread);
        _responseThread.Start(); // start the response thread
    }

    private static void ResponseThread()
    {
        while (true)
        {
            HttpListenerContext context = _httpListener.GetContext(); // Get a context and you'll find the request URL in context.Request.Url

            //byte[] _responseArray = Encoding.UTF8.GetBytes("<html><head><title>Localhost server -- port 5000</title></head>" +
            //    "<body>Welcome to the <strong>Localhost server</strong> -- <em>port 5000!</em></body></html>"); // get the bytes to response

            //context.Response.OutputStream.Write(_responseArray, 0, _responseArray.Length); // write bytes to the output stream

            //context.Response.KeepAlive = false; // set the KeepAlive bool to false

            context.Response.Close(); // close the connection

            if (context != null && context.Request != null && context.Request.Url != null)
            {
                var url = context.Request.Url.ToString();
                Debug.Log(url);
                var code = GetCode(url);

                if (!String.IsNullOrEmpty(code))
                {
                    //Debug.Log(code);
                    TokenDataHolder.urlCode = code;
                }

            }
        }
    }

    private static string GetCode(string urlParam)
    {
        //string testStr = "http://localhost:5000/?code=ab28ca9222e443ee9d3c7c84d429a8d7";
        //string testStr1 = "nikola";

        int position = urlParam.IndexOf(sample);
        if (position < 0)
            return string.Empty;
        else
        {
            var code = urlParam.Substring(position + sample.Length);
            return code;
        }
    }

}
