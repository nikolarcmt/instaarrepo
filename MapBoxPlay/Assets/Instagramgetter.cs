﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
using Mapbox.Unity.Map;
using System;
using Mapbox.Utils;
using Assets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts;

public class Instagramgetter : MonoBehaviour {

    public AbstractMap _map;

    public static List<string> locationsLatLng = new List<string>();
    public static List<string> nameLatLng = new List<string>();

    public static List<Vector2d> vectorLatLng = new List<Vector2d>();

    int cntEnd = 0;

    public GameObject manuBar;
    public GameObject Loader;

    public static string readAccToken;

    // Use this for initialization
    void Start () {
  
        
    }
    public void LoadLatLng()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            //compassValTMP = data.accToken; //ovde smesti acc token
            readAccToken = data.accToken;
        }
    }


    IEnumerator GetAllPictureLocation()
    {

        Loader.SetActive(true);

        LoadLatLng();

        //ovde smesti acc token
        //string url1 = "https://api.instagram.com/v1/media/search?lat=43.3277501&lng=21.9015916&distance=100000&access_token=534828982.c8863a9.cb52d820cdd840e19a2969df06e60ca0";
        //string url1 = "https://api.instagram.com/v1/users/self/media/recent/?max_id=200&access_token=534828982.c8863a9.cb52d820cdd840e19a2969df06e60ca0";

        //string GetUserDataUrl = "https://api.instagram.com/v1/users/self/media/recent/?max_id=200&access_token=" + readAccToken;

        string GetUserDataUrl = StringResources.GetUserDataUrl + readAccToken;

        WWW www = new WWW(GetUserDataUrl);
        //43.3277501,21.9015916
        yield return www;

        string api_response = www.text;
        Debug.Log(api_response);

        IDictionary apiParse = (IDictionary)Json.Deserialize(api_response);
        IList instagramPicturesList = (IList)apiParse["data"];

        Debug.Log("Instagram pictures list count: " + instagramPicturesList.Count);

        foreach (IDictionary instagramPicture in instagramPicturesList)
        {
            cntEnd++;
            //main picture info
            IDictionary images = (IDictionary)instagramPicture["images"];
            IDictionary standardResolution = (IDictionary)images["standard_resolution"];
            string mainPic_url = (string)standardResolution["url"];
            //Debug.Log(mainPic_url);

            //location info
            WWW mainPic = new WWW(mainPic_url);
            yield return mainPic;
            IDictionary location = (IDictionary)instagramPicture["location"];
            try
            {

                double lat = (double)location["latitude"];
                double lon = (double)location["longitude"];
                string placeName = (string)location["name"];
                Debug.Log("Coordinates: " + lat + ", " + lon);

                var tmpLocationLatLng = lat.ToString() + ", " + lon.ToString();

                locationsLatLng.Add(tmpLocationLatLng);

                var tmpPlaceName = placeName.ToString();

                nameLatLng.Add(tmpPlaceName);

                Vector2d tmpVector = new Vector2d(lat, lon);

                vectorLatLng.Add(tmpVector);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }


            if (cntEnd == instagramPicturesList.Count)
            {
                cntEnd = 0;
                manuBar.SetActive(true);
                Loader.SetActive(false);
            }

        }

        _map.transform.position = new Vector3(0, -0.03f, 0);
        //Debug.Log("Full list" + locationsLatLng[1].ToString());
    }

    // Update is called once per frame
    void Update () {
       

        if(TokenDataHolder.changeScreen == true)
        {
            StartCoroutine(GetAllPictureLocation());
            TokenDataHolder.changeScreen = false;

        }

    }
}
